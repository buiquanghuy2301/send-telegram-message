# Send Telegram message  

**Step 1:**
In Telegram, search BotFather and start the chat.

![alt text](./img/step1.png)

**Step 2:**

- Send the `/newbot` command to create new chat bot.
- Choose a name for your bot.
- Choose a username for your bot.

After that, the bot is created. You will receive a message with a link to your bot t.me/<bot_username>, and API token.

![alt text](./img/step2.png)

**Step 3:**
Add the chat bot into your group or chanel.

![alt text](./img/step3.png)

**Step 4:**
Send `/my_id @<your_bot_username>` to start chat bot

![alt text](./img/step4.png)

**Step 5:**
Install required packages:

```
python3 -m venv .venv_telegram_bot && \
. .venv_telegram_bot/bin/activate && \
pip install -r requirements.txt
```

**Step 6:**
Put your chat bot API token into config.cfg file

![alt text](./img/step6.png)

**Step 7:**
Grant your content to `message` variable.

![alt text](./img/step7.png)