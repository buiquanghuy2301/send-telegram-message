import requests

def get_chat_id(API_TOKEN:str):
    bot_updates = requests.get(f"https://api.telegram.org/bot{API_TOKEN}/getUpdates").json()["result"]
    for i in range(len(bot_updates)):
        if "my_chat_member" in bot_updates[i].keys():
            chat_id = bot_updates[i]["my_chat_member"]["chat"]["id"]
            break
    return chat_id
    