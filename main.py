
import configparser
import get_info
import send

config = configparser.ConfigParser()
config.read("./config.cfg")
API_TOKEN = config['TELEGRAM_BOT']['API_TOKEN']

def main(API_TOKEN):
    chat_id = get_info.get_chat_id(API_TOKEN=API_TOKEN)        
    send.send_message(API_TOKEN=API_TOKEN,
                      chat_id=chat_id,
                      message="test123",
                      parse_mode="Markdown")
    send.send_photo(API_TOKEN=API_TOKEN,
                    chat_id=chat_id,file_path='./img/step3.png',
                    caption="send photo test",
                    parse_mode="Markdown")
    send.send_document(API_TOKEN=API_TOKEN,
                       chat_id=chat_id,
                       file_path='./requirements.txt',
                       parse_mode="Markdown")
    
main(API_TOKEN=API_TOKEN)