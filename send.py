import telegram

def send_message(API_TOKEN:str,
                 chat_id:str,
                 message:str,
                 parse_mode:str):
    try:
        telegram_notify = telegram.Bot(API_TOKEN)
        telegram_notify.send_message(
            chat_id=chat_id,
            text=message,
            parse_mode=parse_mode)
    except Exception as ex:
        print(ex)

def send_photo(API_TOKEN: str,
               chat_id:str,
               file_path: str = None,
               url: str = None,
               caption: str = None,
               parse_mode:str = None):
    try:
        telegram_notify = telegram.Bot(API_TOKEN)
        if file_path:
            photo = open(file_path,"rb")
            telegram_notify.send_photo(
                chat_id=chat_id,
                photo=photo,
                caption = caption,
                parse_mode=parse_mode)
        else:
            telegram_notify.send_photo(
                chat_id=chat_id,
                photo=url,
                caption = caption,
                parse_mode=parse_mode)
    except Exception as ex:
        print(ex)
        
def send_document(API_TOKEN: str,
                chat_id: str,
                file_path: str = None,
                url: str = None,
                caption: str = None,
                parse_mode:str = None):
    try:
        telegram_notify = telegram.Bot(API_TOKEN)
        if file_path:
            document = open(file_path,"rb")
            telegram_notify.send_document(
                chat_id=chat_id,
                document=document,
                caption = caption,
                parse_mode=parse_mode)
        else:
            telegram_notify.send_document(
                chat_id=chat_id,
                document=url,
                caption = caption,
                parse_mode=parse_mode)
    except Exception as ex:
        print(ex)